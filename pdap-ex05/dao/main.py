from user import User
from userdao import UserDao

user = User(-1,"Peter","123")
userDao = UserDao.getInstance()
userDao.insert(user) 
print(user)
user.passwd = "234"
userDao.update(user)
print(user)
user_from_db = userDao.get(1)
print(user_from_db)
all_users_from_db = userDao.get()
print(all_users_from_db)

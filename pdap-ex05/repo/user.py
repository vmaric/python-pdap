class User:
    def __init__(self,id,username,passwd):
        self.id = id
        self.username = username
        self.passwd = passwd

    def __str__(self):
        return f"{self.id} {self.username} {self.passwd}"
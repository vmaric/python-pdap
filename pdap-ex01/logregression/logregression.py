import os
import math

#CLASS
class LogisticRegression:
    outputs = []
    inputs = []
    def __init__(self):
        pass
    def add_entry(self,x,y):
        self.inputs.append(x)
        self.outputs.append(y)
    def sig(self,p):
        return 1 / (1 + math.exp(-p))
    def predict_next(self,x):
        if len(self.inputs) < 1:
            return 0
        try:
            sum_x   = sum(self.inputs)
            sum_y   = sum(self.outputs)
            sum_xy  = sum([self.inputs[i]*self.outputs[i] for i in range(len(self.inputs))])
            sumx2   = sum([i**2 for i in self.inputs])
            sumy2   = sum([i**2 for i in self.outputs])
            size    = len(self.inputs)  
            a       = ((sum_y * sumx2) - (sum_x * sum_xy)) / (size * sumx2 - (sum_x ** 2))
            b       = (size * sum_xy - sum_x * sum_y) / (size * sumx2 - (sum_x**2)) 
            prediction = a + (b*x)
            prediction = self.sig(prediction)
            prediction = math.log(prediction/(1-prediction))
        except:
            prediction = 0
        return prediction  




from entity import Entity

class User(Entity): 

    def getkey(self):
        return "id"
    
    def gettable(self):
        return "user" 

    def __init__(self,id=None,username=None,passwd=None):
        self.id = id
        self.username = username
        self.passwd = passwd

    def __str__(self):
        return f"{self.id} {self.username} {self.passwd}"
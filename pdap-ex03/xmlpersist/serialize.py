from person import Person

import xml.etree.ElementTree as xml

import os

os.chdir("pdap-ex03/xmlpersist") 

p = Person("Peter") 
 
root = xml.Element("person")
name = xml.SubElement(root,"name")
name.text = p.username  
tree = xml.ElementTree(root)

tree.write("person.xml")


import pandas as pd  
tbl = pd.read_csv("../gamestats.txt")
tbl = tbl[["player_id","game"]]
tbl = tbl.groupby("player_id").count()
tbl = tbl.sort_values("game", ascending=False)
print(tbl.head(10))
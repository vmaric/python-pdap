class Person:
    def __init__(self,name,age,pid):
        self.name = name
        self.age = age
        self.pid = pid

    def save(self):
        file = open("person","w")
        file.write(f"{self.name},{self.age},{self.pid}")
        file.close()

    @staticmethod
    def load():
        file = open("person","r")
        line = file.readline().split(",")
        file.close()
        return Person(line[0],line[1],line[2])
        
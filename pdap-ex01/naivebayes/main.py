import numpy as np
from naivebayes import NaiveBayes

nb = NaiveBayes(np.array([[1,1,1,1],[0,0,0,0]]),[("Over 3h",0),("Is tank",1),("Is premade",2)],3)
while True:
    try:
        entry = [
            1 if input("You practice over 3h? ") == "1" else 0,
            1 if input("You play tank? ") == "1" else 0,
            1 if input("You play with someone? ") == "1" else 0
        ]
        try:
            nb.predict(entry)
            print("Prediction:","You will","win" if nb.outcome else "not win")
        except Exception as ex:
            print("Cannot predict yet")
        entry.append(1 if input("You won? ") == "1" else 0)
        nb.model = np.concatenate((nb.model,[entry]))
    except Exception as ex:
        print("Invalid entry")
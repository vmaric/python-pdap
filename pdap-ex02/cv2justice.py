import cv2
import os
from sklearn.svm import SVC
import numpy as np 
slike = ("json.jpg","json1.jpg","ron.jpg","ron1.jpg","tracy.jpg","tracy1.jpg",)
klase = [0,0,1,1,2,2]
label = ("Json","Ron","Tracy") 
x = []  
for slika in slike:
    img = cv2.imread(f"pdap-ex02/faces/img/{slika}") 
    img = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
    img = cv2.resize(img,dsize=(50,50))
    img = img.reshape(50*50)
    x.append(img)  
model = SVC()
model.fit(x,klase)  
path = os.path.dirname(cv2.__file__)
face_cascade = cv2.CascadeClassifier(f"{path}/data/haarcascade_frontalface_default.xml")
img = cv2.imread("pdap-ex02/justice.jpg")
gray = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
detect = face_cascade.detectMultiScale(
    gray,1.1,5
) 
for x,y,w,h in detect: 
    img1 = img[y:y+h, x:x+w] 
    img1 = cv2.resize(img1,dsize=(50,50))
    img1 = cv2.cvtColor(img1,cv2.COLOR_RGB2GRAY) 
    img1 = img1.reshape(50*50)
    pred = model.predict([img1]) 
    if label[pred.squeeze()]:
        title = label[pred.squeeze()] 
    cv2.putText(img,title,(x,y+h),cv2.FONT_HERSHEY_PLAIN,1.4,(0,255,255),1,cv2.LINE_AA)  
cv2.imshow("Window",img) 
cv2.waitKey(0)
cv2.destroyAllWindows()

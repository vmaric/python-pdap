import numpy as np
#over3h 	  tank    premade    pobeda
class NaiveBayesNode:
    def __init__(self,name,index,outcome_index):
        self.name               = name
        self.index              = index
        self.outcome_index      = outcome_index
        self.prob_positive      = 0
        self.prob_negative      = 0
        self.total_positives    = 0
        self.total_negatives    = 0
        self.px                 = 0 
    def calc_probability(self,model,val): 
        total_model_size = len(model)

        total_positives_size    = np.sum(model[:,self.outcome_index])
        total_negatives_size    = total_model_size - total_positives_size 

        target_positives_arr    = np.array([x for x in model[:,[self.index,self.outcome_index]] if x[0] == val])
        total_entries           = len(target_positives_arr)
        total_hits              = len(np.array([x for x in target_positives_arr if x[1] == 1]))
        total_misses            = total_entries - total_hits

        self.prob_positive      = total_hits / total_positives_size
        self.prob_negative      = total_misses / total_negatives_size

        self.px =               total_entries / total_model_size

    def __str__(self):
        return f"{self.name}: {self.prob_positive},{self.prob_negative},{self.px}"

class NaiveBayes:  
    def __init__(self,model,nodes,outcome_index):
        self.model              = model
        self.outcome_index      = outcome_index
        self.nodes              = [NaiveBayesNode(n[0],n[1],outcome_index) for n in nodes]  
        self.outcome            = None
        self.positive           = 0
        self.negative           = 0
        self.px                 = 0

    def predict(self,values,model=np.array([])):
        
        if model.size > 0:
            self.model = model
        if self.model.size < 1:
            self.outcome = False
            return

        total_model_size = len(self.model)
        total_positives_count   = np.sum(self.model[:,self.outcome_index])
        total_positives_size    = total_positives_count / total_model_size
        total_negatives_size    = (total_model_size - total_positives_count) / total_model_size
        for node in self.nodes:
            node.calc_probability(self.model,values[node.index]) 
        self.positive = np.prod([p.prob_positive for p in self.nodes])*total_positives_size
        self.negative = np.prod([p.prob_negative for p in self.nodes])*total_negatives_size
        self.px         = np.prod([node.px for node in self.nodes])

        self.outcome = (self.positive / self.px) > (self.negative / self.px)
        return self.outcome 

    def __str__(self):
        return f"outcome: {self.outcome},{self.positive},{self.negative}"





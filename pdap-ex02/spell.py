import torch
import torch.nn as nn
import cv2
import os

files = os.listdir("spells")
titles = []
data = []
for i in files:
    titles.append(i.replace(".jpg",""))
    img = cv2.imread("spells/"+i)
    data.append(img.reshape(72*72*3))  
 
data = torch.tensor(data,dtype=torch.float32)
target = torch.tensor([0,1,2,3,4,5,6,7,8,9,10,11])

class MyModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.lin = nn.Linear(72*72*3,12) 
    def forward(self, x):
        out = self.lin(x)
        return out

model = MyModel()
loss_f = nn.CrossEntropyLoss()
opt = torch.optim.SGD(model.parameters(),lr=0.01)
for i in range(1000):
    pred = model(data)
    loss = loss_f(pred,target)
    _,pred = torch.max(pred,1) 
    loss.backward()
    opt.step()
    opt.zero_grad() 

img = cv2.imread("spells/flash.jpg")
img = torch.tensor(img.reshape(72*72*3),dtype=torch.float32)
pred = model(img)
_,pred = torch.max(pred,0)
print(titles[pred])

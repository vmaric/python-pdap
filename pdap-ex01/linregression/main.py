from linregression import LinearRegression
#MAIN
lr = LinearRegression() 

file = None
try:
    file = open("regression.csv")
    line = file.readline()
    while line:
        age,bet = line.strip().split(",")
        lr.add_entry(int(age),float(bet))
        line = file.readline()
except:
    print("Data file not exist")
finally:
    if file:
        file.close() 
file = None

while True: 
    age = int(input("Age: "))
    print(f"You should probably bet {lr.predict_next(age):.2f}")
    bet = float(input("Bet: "))
    lr.add_entry(age,bet)
    try:
        file = open("regression.csv","a")
        file.write(f"{age},{bet}\r\n")
        file.flush()
    except:
        print("Data file not opened successfully")
    finally:
        if file:
            file.close()
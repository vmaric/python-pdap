import numpy as np
import torch.nn as nn
import torch
import cv2
import urllib.request as req
 
demos = ("json.jpg","tracy.jpg","ron.jpg")
classes = ("Json","Tracy","Ron") 
images = (
    ("json.jpg",0),
    ("json1.jpg",0),
    ("tracy.jpg",1),
    ("tracy1.jpg",1),
    ("ron.jpg",2),
    ("ron1.jpg",2)
)

data = []
target = []
for i,cl in images:
    img = cv2.imread(f"img/{i}")
    img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    img = cv2.resize(img,dsize=(32,32))
    img = img.reshape(1,32,32) 
    data.append(img)
    target.append(cl)

data = torch.tensor(data,dtype=torch.float32)  

target = torch.tensor(target)
 
loss_f = nn.CrossEntropyLoss()  
maxpool = nn.MaxPool2d(2,2)
lin = nn.Linear(20*2*2,3)
opt = torch.optim.SGD(lin.parameters(),lr=0.01)
relu = nn.ReLU()
conv = nn.Conv2d(1,10,8) 
conv1 = nn.Conv2d(10,20,8) 

for i in range(1000):
    pred = conv(data)
    pred = relu(pred)
    pred = maxpool(pred)
    pred = conv1(pred)
    pred = relu(pred)
    pred = maxpool(pred) 
    pred = pred.reshape(-1,20*2*2)
    pred = lin(pred)

    loss = loss_f(pred,target)
    loss.backward()
    opt.step()
    opt.zero_grad()  

while True:
    res = req.urlopen(input("Unesi adresu slike:")) 
    img_array = np.array(bytearray(res.read()), dtype=np.uint8)
    img = cv2.imdecode(img_array, -1)

    img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    img = cv2.resize(img,dsize=(32,32))
    img = img.reshape(1,32,32) 

    img = torch.tensor([img],dtype=torch.float32) 

    prediction = None
    with torch.no_grad():
        pred = conv(img)
        pred = relu(pred) 
        pred = maxpool(pred) 
        pred = conv1(pred)
        pred = relu(pred)
        pred = maxpool(pred)
        pred = pred.reshape(-1,20*2*2) 
        pred = lin(pred) 
        v, prediction = torch.max(pred,1)
        print(prediction,classes[prediction])
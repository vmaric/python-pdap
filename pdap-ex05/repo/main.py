from user import User
from repo import Database

user = User(-1,"Peter","123")
Database.insert_user(user)
print(user)
user.passwd = "234"
Database.update_user(user)
print(user)
user_from_db = Database.get_user(1)
print(user_from_db)
all_users_from_db = Database.get_user()
print(all_users_from_db)

import cv2
import os

path = os.path.dirname(cv2.__file__) 

face_cascade = cv2.CascadeClassifier(
    f"{path}/data/haarcascade_frontalface_default.xml"
)

img = cv2.imread("pdap-ex02/aquaman.jpg")
replacement = cv2.imread("pdap-ex02/ronA.png")
gray = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
detect = face_cascade.detectMultiScale(
    gray,1.3,10,
    minSize=(50,50),
    maxSize=(200,200)
) 
for x,y,w,h in detect: 
    replacement = cv2.resize(replacement,dsize=(w,h+30))
    original = img[y-30:y+h,x:x+w]
    for i,v in enumerate(replacement):
        for j,v1 in enumerate(v):
            if v1.sum() == 0:
                replacement[i][j] = original[i][j] 
    img[y-30:y+h,x:x+w] = replacement 
cv2.imshow("Window",img)
cv2.waitKey(0)
cv2.destroyAllWindows()

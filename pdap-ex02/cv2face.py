import cv2
import matplotlib.pyplot as plt
from sklearn.svm import SVC
import numpy as np 
slike = ("json.jpg","json1.jpg","ron.jpg","ron1.jpg","tracy.jpg","tracy1.jpg",)
klase = [0,0,1,1,2,2]
label = ("Json","Ron","Tracy") 
x = []  
for slika in slike:
    img = cv2.imread(f"pdap-ex02/faces/img/{slika}") 
    img = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
    img = cv2.resize(img,dsize=(50,50))
    img = img.reshape(50*50)
    x.append(img) 
model = SVC()
model.fit(x,klase) 
img = cv2.imread(f"pdap-ex02/faces/img/jsontest.jpg") 
img = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
img = cv2.resize(img,dsize=(50,50))
img = img.reshape(50*50)
x.append(img) 
pred = model.predict([img])
print(label[pred.squeeze()])


 
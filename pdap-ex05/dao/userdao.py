from database import Database
from user import User

class UserDao: 
    __instance = None 
    @staticmethod
    def getInstance():
        if not UserDao.__instance:
            UserDao.__instance = UserDao()
        return UserDao.__instance

    def __init__(self):
        if not UserDao.__instance:
            UserDao.__instance = self
        else:
            raise Exception("Cannot instantiate")

    def insert(self,user):
        cur = Database.getconnection().cursor()
        cur.execute("insert into user (username,passwd) values (%s,%s)", (user.username,user.passwd)) 
        user.id = cur.lastrowid
        cur.close() 

    def update(self,user):
        cur = Database.getconnection().cursor()
        cur.execute("update user set username = %s, passwd = %s where id = %s", (user.username,user.passwd, user.id))  
        cur.close()

    def delete(self,id):
        cur = Database.getconnection().cursor()
        cur.execute("delete from user where id = %s", (id))  
        cur.close()

    def get(self,id=None):
        res = None
        cur = Database.getconnection().cursor()
        if id != None: 
            cur.execute("select * from user where id = %s", (id,))
            usr = cur.fetchone()
            if usr:
                res = User(usr[0],usr[1],usr[2]) 
        else:
            res = []
            cur.execute("select * from user")
            for usr in cur.fetchall():
                res.append(User(usr[0],usr[1],usr[2]))
        cur.close()
        return res

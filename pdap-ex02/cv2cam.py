import cv2
import os

path = os.path.dirname(cv2.__file__) 

face_cascade = cv2.CascadeClassifier(
    f"{path}/data/haarcascade_frontalface_default.xml"
)
replacement = cv2.imread("pdap-ex02/ronA.png") 
cam = cv2.VideoCapture(0) 
while True:
    _,img = cam.read() 
    gray = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
    detect = face_cascade.detectMultiScale(
        gray,1.1,18
    ) 
    for x,y,w,h in detect:   
        repl = cv2.resize(replacement,dsize=(w-10,h+30))
        original = img[y-10:y+h+20,x+5:x+w-5]
        for i,v in enumerate(repl):
            for j,v1 in enumerate(v):
                if v1.sum() < 100:
                    repl[i][j] = original[i][j]
        img[y-10:y+h+20,x+5:x+w-5] = repl 
    cv2.imshow("Window",img) 
    cv2.waitKey(1)
cv2.destroyAllWindows()

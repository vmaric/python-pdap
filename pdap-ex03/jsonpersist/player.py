import json
import copy
from pos import Position

class Player:
    def __init__(self,id,name,pos):
        self.id     = id
        self.name   = name
        self.pos    = pos

    def serialize(self): 
        dct = copy.copy(self.__dict__)
        dct["pos"] = self.pos.__dict__
        return json.dumps(dct)
        
    def save(self):
        file = open("player.json","w")
        file.write(self.serialize())
        file.close()

    @staticmethod
    def load():
        o = json.load(open("player.json","r"))
        return Player(o["id"],o["name"],Position(o["pos"]["x"],o["pos"]["y"]))

    def __str__(self):
        return f"{self.id} {self.name} {self.pos.x} {self.pos.y}"
        
import matplotlib.pyplot as plt
import pandas as pd
tbl = pd.read_csv("../gamestats.txt")
tbl["time"] = pd.to_datetime(tbl["time"]).dt.date 
tbl.columns = ['id','time','player_id','skin','game','session','play_mode']  
tbl = tbl[["game","time"]]
tbl = tbl[(tbl.time >= pd.Timestamp('2019-12-01')) & (tbl.time < pd.Timestamp('2019-12-15'))]
tbl = tbl.groupby("time").count()
tbl.plot(kind="bar",width=.95) 
plt.xticks(rotation=45, ha="right", fontsize=8)
plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.2)
plt.show()

import mysql.connector as connector
import matplotlib.pyplot as plt 

db = connector.connect(
    database="sakila",
    host="localhost",
    username="root",
    passwd=""
) 
cur = db.cursor() 
cur.execute("select count(country.country),country.country from customer join address on customer.address_id = address.address_id join city on address.city_id = city.city_id join country on city.country_id = country.country_id group by city.country_id order by count(country.country) desc limit 10") 
titles = []
customers = [] 
for c,t in cur.fetchall():
    titles.append(t)
    customers.append(c) 
plt.pie(customers,labels=titles,autopct='%1.1f%%') 
plt.show()
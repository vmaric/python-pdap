import cv2
import matplotlib.pyplot as plt
from sklearn.svm import SVC
import numpy as np

img = cv2.imread("pdap-ex02/faces/img/json.jpg")

img = cv2.resize(
    img,dsize=(300,300)
)
img = cv2.rectangle(
    img,
    (50,50),(90,90),
    (0,0,255),2
)
img = cv2.circle(
    img,
    (150,150),50,
    (255,0,0),2
)
img = cv2.putText(
    img, 'This is Json Momoa!',
    (0,220),cv2.QT_FONT_NORMAL,0.8,
    (255,255,0),2,cv2.LINE_AA
)

cv2.imshow("Json",img)
while True:
    cv2.waitKey(0)
cv2.destroyAllWindows()
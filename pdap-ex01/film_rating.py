import mysql.connector as connector
import matplotlib.pyplot as plt 

db = connector.connect(
    database="sakila",
    host="localhost",
    username="root",
    passwd=""
) 
cur = db.cursor() 
cur.execute("select count(*) as cnt, rating from film group by rating")
cnt = []
nm = []

for c,r in cur.fetchall():
    cnt.append(c)
    nm.append(r)
db.close()

plt.bar(nm,cnt)
plt.show()
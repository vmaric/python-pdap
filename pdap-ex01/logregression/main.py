from logregression import LogisticRegression

#MAIN
lr = LogisticRegression() 
import random
for i in range(10000):
    age = random.randint(10,80)
    lr.inputs.append(age)
    lr.outputs.append(1 if age < 30 else 0)

print(lr.predict_next(25))
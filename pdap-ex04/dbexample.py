import mysql.connector as connector

db = connector.connect(host="localhost",database="test",passwd="",username="root") 
cur = db.cursor() 
cur.execute(
    "insert into person (name,age,pid) values (%s,%s,%s)",
    ('Peter',35,123)
)
db.commit()
insert_id = cur.lastrowid
print("Inserted user:",insert_id)
cur.execute(
    "update person set age=%s where person_id = %s",
    (36,insert_id)
)
db.commit()  
print("Updated user:",insert_id)
cur.execute("select * from person")
for person in cur.fetchall():
    print(person)
db.close()


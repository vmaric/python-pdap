import cv2
import os

path = os.path.dirname(cv2.__file__)

face_cascade = cv2.CascadeClassifier(f"{path}/data/haarcascade_frontalface_default.xml")
img = cv2.imread("pdap-ex02/aquaman.jpg")
gray = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
detect = face_cascade.detectMultiScale(
    gray,1.3,10,
    minSize=(50,50),
    maxSize=(200,200)
) 
for x,y,w,h in detect:
    cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),5)
cv2.imshow("Window",img)
cv2.waitKey(0)
cv2.destroyAllWindows()

import abc 
from database import Database
class Entity(abc.ABC): 
    @abc.abstractmethod
    def gettable(self):
        pass
    @abc.abstractmethod
    def getkey(self):
        pass
    def __setfields(self):
        return ",".join([str(k)+"='"+str(self.__dict__[k])+"'" for k in self.__dict__ if k != self.getkey()])
    def insert(self):
        q = f"insert into {self.gettable()} set {self.__setfields()}"
        conn = Database.getconnection()
        cur = conn.cursor()
        cur.execute(q)
        setattr(self,self.getkey(),cur.lastrowid)
        cur.close()

    def update(self):
        keyval = getattr(self,self.getkey())
        q = f"update {self.gettable()} set {self.__setfields()} where {self.getkey()}={keyval}"
        conn = Database.getconnection()
        cur = conn.cursor()
        cur.execute(q)
        cur.close()

    def delete(self):
        keyval = getattr(self,self.getkey())
        q = f"delete from {self.gettable()} where {self.getkey()}={keyval}"
        conn = Database.getconnection()
        cur = conn.cursor()
        cur.execute(q)
        cur.close()

    @classmethod
    def get(cls,keyval):
        instance = cls()
        table = instance.gettable()
        keycol = instance.getkey() 
        q = f"select * from {table} where {keycol} = {keyval}"
        conn = Database.getconnection()
        cur = conn.cursor()
        cur.execute(q)
        colnames = [n[0] for n in cur.description]
        row = cur.fetchone() 
        if row:
            for i,fieldval in enumerate(row):
                setattr(instance,colnames[i],fieldval)
        else:
            instance = None
        cur.close()
        return instance

    @classmethod
    def getmany(cls,filter=""):
        res = []
        instance = cls()
        table = instance.gettable()
        keycol = instance.getkey() 
        q = f"select * from {table} {filter}".strip()
        conn = Database.getconnection()
        cur = conn.cursor()
        cur.execute(q)
        colnames = [n[0] for n in cur.description]
        for row in cur.fetchall(): 
            for i,fieldval in enumerate(row):
                instance = cls()
                setattr(instance,colnames[i],fieldval)
                res.append(instance)
        cur.close()
        return res
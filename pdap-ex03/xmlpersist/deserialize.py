from person import Person
import xml.etree.ElementTree as xml
import os

os.chdir("pdap-ex03/xmlpersist")  

tree = xml.parse(
    open("person.xml","r")
)
p = Person(tree.getroot()[0].text)
print(p.username)
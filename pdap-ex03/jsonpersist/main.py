from player import Player
from pos import Position
import os 
os.chdir("pdap-ex03/jsonpersist")

# Saving player
pl = Player(1,"peter",Position(10,20))  
pl.save()

# Loading player
pl = Player.load()
print(pl)
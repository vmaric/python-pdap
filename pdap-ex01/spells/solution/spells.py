import matplotlib.pyplot as plt
import matplotlib.image as img
import os 
files = filter(lambda a:a.endswith(".jpg"),os.listdir())
fig = plt.figure("LoL spells", figsize=(3,4))
cnt = 1
for i in files:
    ax = plt.subplot(4,3,cnt)
    ax.imshow(img.imread(i))
    cnt+=1 
plt.show()



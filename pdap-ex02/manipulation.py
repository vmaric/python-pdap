import cv2
import matplotlib.pyplot as plt
from sklearn.svm import SVC
import numpy as np

img = cv2.imread("pdap-ex02/faces/img/json.jpg")

img = cv2.resize(
    img,dsize=(300,300)
)
img[130:160] = np.random.randint(
    255,size=(30,300,3)
)

cv2.imshow("Json",img)
cv2.waitKey(0)
cv2.destroyAllWindows()
import mysql.connector as connector
from user import User

class Database:
    connection = None 
    @staticmethod
    def getconnection():
        if not Database.connection:
            Database.connection = connector.connect(
                host="localhost",database="test",passwd="",username="root"
            )
            Database.connection.autocommit = True
        return Database.connection

    @staticmethod
    def insert_user(user):
        cur = Database.getconnection().cursor()
        cur.execute("insert into user (username,passwd) values (%s,%s)", (user.username,user.passwd)) 
        user.id = cur.lastrowid
        cur.close()

    @staticmethod
    def update_user(user):
        cur = Database.getconnection().cursor()
        cur.execute("update user set username = %s, passwd = %s where id = %s", (user.username,user.passwd, user.id))  
        cur.close()

    @staticmethod
    def delete_user(id):
        cur = Database.getconnection().cursor()
        cur.execute("delete from user where id = %s", (id))  
        cur.close()

    @staticmethod
    def get_user(id=None):
        res = None
        cur = Database.getconnection().cursor()
        if id != None: 
            cur.execute("select * from user where id = %s", (id,))
            usr = cur.fetchone()
            if usr:
                res = User(usr[0],usr[1],usr[2]) 
        else:
            res = []
            cur.execute("select * from user")
            for usr in cur.fetchall():
                res.append(User(usr[0],usr[1],usr[2]))
        cur.close()
        return res

import mysql.connector as connector

class Database:
    connection = None 
    @staticmethod
    def getconnection():
        if not Database.connection:
            Database.connection = connector.connect(
                host="localhost",database="test",passwd="",username="root"
            )
            Database.connection.autocommit = True
        return Database.connection
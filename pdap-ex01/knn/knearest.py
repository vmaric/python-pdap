import math  
class KNN: 
    def __init__(self):
        self.data = [[],[]]

    def __getdistance(self,a,b):
        return math.sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2)
    
    def fit(self,data):
        self.data = data

    def predict(self,newdata):
        distances = []
        for i in [0,1]:
            for j in self.data[i]:
                distances.append((self.__getdistance(newdata,j),i))  
        distances.sort() 
        distances = len([x for x in distances[:5] if x[1] == 1]) 
        predicted_class = 1 if distances>2 else 0
        self.data[predicted_class].append(newdata) 
        return predicted_class


